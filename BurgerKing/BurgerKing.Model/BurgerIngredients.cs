﻿using System.Collections.Generic;
using System.Runtime.Serialization;

namespace BurgerKing.Model
{
    [DataContract]
    public class BurgerIngredients : Entity
    {
        [DataMember]
        public ICollection<Ingredient> Malt { get; set; }

        [DataMember]
        public ICollection<Ingredient> Hops { get; set; }

        [DataMember]
        public string Yeast { get; set; }

        public Burger Burger { get; set; }
    }
}