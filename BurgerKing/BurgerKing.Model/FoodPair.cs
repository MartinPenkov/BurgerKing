﻿using System;
using System.Collections.Generic;
using System.Runtime.Serialization;
using System.Text;

namespace BurgerKing.Model
{
    [DataContract]
    public class FoodPair : Entity
    {
        [DataMember]
        public string Value { get; set; }

        public Burger Burger { get; set; }
    }
}
