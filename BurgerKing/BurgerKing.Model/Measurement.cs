﻿using System.Runtime.Serialization;

namespace BurgerKing.Model
{
    [DataContract]
    public class Measurement : Entity
    {
        [DataMember]
        public double Value { get; set; }

        [DataMember]
        public string Unit { get; set; }

        public Tempreture Temperature { get; set; }
        public Ingredient Ingredient { get; set; }
        public Burger VolumeBurger { get; set; }
        public Burger BoilVolumeBurger { get; set; }
    }
}