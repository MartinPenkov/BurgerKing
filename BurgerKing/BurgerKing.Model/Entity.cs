﻿namespace BurgerKing.Model
{
    public abstract class Entity
    {
        public virtual int Id { get; set; }
    }
}
