﻿using System.Runtime.Serialization;

namespace BurgerKing.Model
{
    [DataContract]
    public class Ingredient : Entity
    {
        [DataMember]
        public string Name { get; set; }

        [DataMember]
        public Measurement Amount { get; set; }

        [DataMember]
        public string Add { get; set; }

        [DataMember]
        public string Attribute { get; set; }

        public int? AmountId { get; set; }
        public BurgerIngredients BurgerIngredientMalt { get; set; }
        public BurgerIngredients BurgerIngredientHops { get; set; }
    }
}