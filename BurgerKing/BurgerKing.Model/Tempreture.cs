﻿using Newtonsoft.Json;
using System.Runtime.Serialization;

namespace BurgerKing.Model
{
    [DataContract]
    public class Tempreture : Entity
    {
        [DataMember]
        [JsonProperty(PropertyName = "temp")]
        public Measurement Measurement { get; set; }

        [DataMember]
        public double Duration { get; set; }

        public int? MeasurementId { get; set; }
        public Method MashMethod { get; set; }
        public Method FermentationMethod { get; set; }
    }
}