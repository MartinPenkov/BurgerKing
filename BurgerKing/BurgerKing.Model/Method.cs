﻿using Newtonsoft.Json;
using System.Runtime.Serialization;

namespace BurgerKing.Model
{
    [DataContract]
    public class Method : Entity
    {
        [DataMember]
        [JsonProperty(PropertyName = "mash_temp")]
        public Tempreture MashTemperature { get; set; }

        [DataMember]
        public Tempreture Fermentation { get; set; }

        [DataMember]
        public string Twist { get; set; }

        public int? MashTemperatureId { get; set; }
        public int? FermentationId { get; set; }
        public Burger Burger { get; set; }
    }
}