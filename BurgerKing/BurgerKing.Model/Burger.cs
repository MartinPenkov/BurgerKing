﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Runtime.Serialization;
using System.Text;
using System.Linq;

namespace BurgerKing.Model
{
    [DataContract]
    public class Burger : Entity
    {
        [DataMember]
        public override int Id { get; set; }

        [DataMember]
        public string Name { get; set; }

        [DataMember]
        public string Tagline { get; set; }

        [DataMember]
        [JsonProperty(PropertyName = "first_brewed")]
        public string FirstBrewed { get; set; }

        [DataMember]
        public string Description { get; set; }

        [DataMember]
        [JsonProperty(PropertyName = "image_url")]
        public string ImageUrl { get; set; }

        [DataMember]
        public double Abv { get; set; }

        [DataMember]
        public double Ibu { get; set; }

        [DataMember]
        [JsonProperty(PropertyName = "target_fg")]
        public double TargetFG { get; set; }

        [DataMember]
        [JsonProperty(PropertyName = "target_og")]
        public double TargetOG { get; set; }

        [DataMember]
        public double Ebc { get; set; }

        [DataMember]
        public double Srm { get; set; }

        [DataMember]
        public double Ph { get; set; }

        [DataMember]
        [JsonProperty(PropertyName = "attenuation_level")]
        public double AttenuationLevel { get; set; }

        [DataMember]
        public Measurement Volume { get; set; }

        [DataMember]
        [JsonProperty(PropertyName = "boil_volume")]
        public Measurement BoilVolume { get; set; }

        [DataMember]
        public Method Method { get; set; }

        [DataMember]
        public BurgerIngredients Ingredents { get; set; }

        [DataMember]
        [JsonProperty(PropertyName = "food_pairing")]
        public ICollection<FoodPair> FoodPairs { get; set; }

        [DataMember]
        [JsonProperty(PropertyName = "brewers_tips")]
        public string BrewersTips { get; set; }

        [DataMember]
        [JsonProperty(PropertyName = "contributed_by")]
        public string ContributedBy { get; set; }
        
        public int? VolumeId { get; set; }
        public int? BoilVolumeId { get; set; }
        public int? MethodId { get; set; }
        public int? BurgerIngredientsId { get; set; }
    }
}
