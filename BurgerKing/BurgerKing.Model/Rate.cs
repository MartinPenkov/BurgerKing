﻿using System;

namespace BurgerKing.Model
{
    public class Rate : Entity
    {
        public string IpAddress { get; set; }
        public DateTime HitTime { get; set; }
    }
}
