﻿using System;
using System.Linq;
using System.Linq.Expressions;

namespace BurgerKing.Data
{
    public interface IRepository<T>
    {
        T Insert(T entity);
        IQueryable<T> GetAll();
        T GetById(int id);
        void Save();
    }
}
