﻿using BurgerKing.Model;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.ChangeTracking;
using System.Linq;

namespace BurgerKing.Data
{
    public class BurgerRepository : IRepository<Burger>
    {
        protected DbContext dbContext;
        protected DbSet<Burger> burgers;

        public BurgerRepository(DbContext dbContext)
        {
            this.dbContext = dbContext;
            this.burgers = dbContext.Set<Burger>();
        }

        public IQueryable<Burger> GetAll()
        {
            return dbContext.Set<Burger>()
                .Include(b => b.FoodPairs)
                .Include(b => b.Volume)
                .Include(b => b.BoilVolume)
                .Include(b => b.Method)
                    .ThenInclude(m => m.MashTemperature)
                        .ThenInclude(t => t.Measurement)
                .Include(b => b.Method)
                    .ThenInclude(m => m.Fermentation)
                        .ThenInclude(t => t.Measurement)
                .Include(b => b.Ingredents)
                    .ThenInclude(i => i.Hops)
                        .ThenInclude(h => h.Amount)
                .Include(b => b.Ingredents)
                    .ThenInclude(i => i.Malt)
                        .ThenInclude(h => h.Amount);
        }

        public Burger GetById(int id)
        {
            return this.GetAll().FirstOrDefault(x => x.Id == id);
                
        }

        public Burger Insert(Burger entity)
        {
            EntityEntry<Burger> entityEntry = this.burgers.Add(entity);

            return entityEntry.Entity;
        }

        public void Save()
        {
            this.dbContext.SaveChanges();
        }
    }
}
