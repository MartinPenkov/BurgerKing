﻿using System;
using System.Collections.Generic;
using Microsoft.EntityFrameworkCore.Migrations;
using Microsoft.EntityFrameworkCore.Metadata;

namespace BurgerKing.Data.Migrations
{
    public partial class CreateBurgerEntities : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "BurgerIngredients",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Yeast = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_BurgerIngredients", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Measurements",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Unit = table.Column<string>(nullable: true),
                    Value = table.Column<double>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Measurements", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Ingredients",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Add = table.Column<string>(nullable: true),
                    AmountId = table.Column<int>(nullable: true),
                    Attribute = table.Column<string>(nullable: true),
                    BurgerIngredientHopsId = table.Column<int>(nullable: true),
                    BurgerIngredientMaltId = table.Column<int>(nullable: true),
                    Name = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Ingredients", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Ingredients_Measurements_AmountId",
                        column: x => x.AmountId,
                        principalTable: "Measurements",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Ingredients_BurgerIngredients_BurgerIngredientHopsId",
                        column: x => x.BurgerIngredientHopsId,
                        principalTable: "BurgerIngredients",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Ingredients_BurgerIngredients_BurgerIngredientMaltId",
                        column: x => x.BurgerIngredientMaltId,
                        principalTable: "BurgerIngredients",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "Temperatures",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Duration = table.Column<double>(nullable: false),
                    MeasurementId = table.Column<int>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Temperatures", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Temperatures_Measurements_MeasurementId",
                        column: x => x.MeasurementId,
                        principalTable: "Measurements",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "Methods",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    FermentationId = table.Column<int>(nullable: true),
                    MashTemperatureId = table.Column<int>(nullable: true),
                    Twist = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Methods", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Methods_Temperatures_FermentationId",
                        column: x => x.FermentationId,
                        principalTable: "Temperatures",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Methods_Temperatures_MashTemperatureId",
                        column: x => x.MashTemperatureId,
                        principalTable: "Temperatures",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "Burgers",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Abv = table.Column<double>(nullable: false),
                    AttenuationLevel = table.Column<double>(nullable: false),
                    BoilVolumeId = table.Column<int>(nullable: true),
                    BrewersTips = table.Column<string>(nullable: true),
                    BurgerIngredientsId = table.Column<int>(nullable: true),
                    ContributedBy = table.Column<string>(nullable: true),
                    Description = table.Column<string>(nullable: true),
                    Ebc = table.Column<double>(nullable: false),
                    FirstBrewed = table.Column<string>(nullable: true),
                    Ibu = table.Column<double>(nullable: false),
                    ImageUrl = table.Column<string>(nullable: true),
                    MethodId = table.Column<int>(nullable: true),
                    Name = table.Column<string>(nullable: true),
                    Ph = table.Column<double>(nullable: false),
                    Srm = table.Column<double>(nullable: false),
                    Tagline = table.Column<string>(nullable: true),
                    TargetFG = table.Column<double>(nullable: false),
                    TargetOG = table.Column<double>(nullable: false),
                    VolumeId = table.Column<int>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Burgers", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Burgers_Measurements_BoilVolumeId",
                        column: x => x.BoilVolumeId,
                        principalTable: "Measurements",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Burgers_BurgerIngredients_BurgerIngredientsId",
                        column: x => x.BurgerIngredientsId,
                        principalTable: "BurgerIngredients",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Burgers_Methods_MethodId",
                        column: x => x.MethodId,
                        principalTable: "Methods",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Burgers_Measurements_VolumeId",
                        column: x => x.VolumeId,
                        principalTable: "Measurements",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "FoodPairs",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    BurgerId = table.Column<int>(nullable: true),
                    Value = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_FoodPairs", x => x.Id);
                    table.ForeignKey(
                        name: "FK_FoodPairs_Burgers_BurgerId",
                        column: x => x.BurgerId,
                        principalTable: "Burgers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateIndex(
                name: "IX_Burgers_BoilVolumeId",
                table: "Burgers",
                column: "BoilVolumeId",
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IX_Burgers_BurgerIngredientsId",
                table: "Burgers",
                column: "BurgerIngredientsId",
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IX_Burgers_MethodId",
                table: "Burgers",
                column: "MethodId",
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IX_Burgers_VolumeId",
                table: "Burgers",
                column: "VolumeId",
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IX_FoodPairs_BurgerId",
                table: "FoodPairs",
                column: "BurgerId");

            migrationBuilder.CreateIndex(
                name: "IX_Ingredients_AmountId",
                table: "Ingredients",
                column: "AmountId",
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IX_Ingredients_BurgerIngredientHopsId",
                table: "Ingredients",
                column: "BurgerIngredientHopsId");

            migrationBuilder.CreateIndex(
                name: "IX_Ingredients_BurgerIngredientMaltId",
                table: "Ingredients",
                column: "BurgerIngredientMaltId");

            migrationBuilder.CreateIndex(
                name: "IX_Methods_FermentationId",
                table: "Methods",
                column: "FermentationId",
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IX_Methods_MashTemperatureId",
                table: "Methods",
                column: "MashTemperatureId",
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IX_Temperatures_MeasurementId",
                table: "Temperatures",
                column: "MeasurementId",
                unique: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "FoodPairs");

            migrationBuilder.DropTable(
                name: "Ingredients");

            migrationBuilder.DropTable(
                name: "Burgers");

            migrationBuilder.DropTable(
                name: "BurgerIngredients");

            migrationBuilder.DropTable(
                name: "Methods");

            migrationBuilder.DropTable(
                name: "Temperatures");

            migrationBuilder.DropTable(
                name: "Measurements");
        }
    }
}
