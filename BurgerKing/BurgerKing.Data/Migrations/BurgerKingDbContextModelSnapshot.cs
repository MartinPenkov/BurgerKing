﻿using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Infrastructure;
using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;
using BurgerKing.Data;

namespace BurgerKing.Data.Migrations
{
    [DbContext(typeof(BurgerKingDbContext))]
    partial class BurgerKingDbContextModelSnapshot : ModelSnapshot
    {
        protected override void BuildModel(ModelBuilder modelBuilder)
        {
            modelBuilder
                .HasAnnotation("ProductVersion", "1.1.2")
                .HasAnnotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn);

            modelBuilder.Entity("BurgerKing.Model.Burger", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<double>("Abv");

                    b.Property<double>("AttenuationLevel");

                    b.Property<int?>("BoilVolumeId");

                    b.Property<string>("BrewersTips");

                    b.Property<int?>("BurgerIngredientsId");

                    b.Property<string>("ContributedBy");

                    b.Property<string>("Description");

                    b.Property<double>("Ebc");

                    b.Property<string>("FirstBrewed");

                    b.Property<double>("Ibu");

                    b.Property<string>("ImageUrl");

                    b.Property<int?>("MethodId");

                    b.Property<string>("Name");

                    b.Property<double>("Ph");

                    b.Property<double>("Srm");

                    b.Property<string>("Tagline");

                    b.Property<double>("TargetFG");

                    b.Property<double>("TargetOG");

                    b.Property<int?>("VolumeId");

                    b.HasKey("Id");

                    b.HasIndex("BoilVolumeId")
                        .IsUnique();

                    b.HasIndex("BurgerIngredientsId")
                        .IsUnique();

                    b.HasIndex("MethodId")
                        .IsUnique();

                    b.HasIndex("VolumeId")
                        .IsUnique();

                    b.ToTable("Burgers");
                });

            modelBuilder.Entity("BurgerKing.Model.BurgerIngredients", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<string>("Yeast");

                    b.HasKey("Id");

                    b.ToTable("BurgerIngredients");
                });

            modelBuilder.Entity("BurgerKing.Model.FoodPair", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<int?>("BurgerId");

                    b.Property<string>("Value");

                    b.HasKey("Id");

                    b.HasIndex("BurgerId");

                    b.ToTable("FoodPairs");
                });

            modelBuilder.Entity("BurgerKing.Model.Ingredient", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<string>("Add");

                    b.Property<int?>("AmountId");

                    b.Property<string>("Attribute");

                    b.Property<int?>("BurgerIngredientHopsId");

                    b.Property<int?>("BurgerIngredientMaltId");

                    b.Property<string>("Name");

                    b.HasKey("Id");

                    b.HasIndex("AmountId")
                        .IsUnique();

                    b.HasIndex("BurgerIngredientHopsId");

                    b.HasIndex("BurgerIngredientMaltId");

                    b.ToTable("Ingredients");
                });

            modelBuilder.Entity("BurgerKing.Model.Measurement", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<string>("Unit");

                    b.Property<double>("Value");

                    b.HasKey("Id");

                    b.ToTable("Measurements");
                });

            modelBuilder.Entity("BurgerKing.Model.Method", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<int?>("FermentationId");

                    b.Property<int?>("MashTemperatureId");

                    b.Property<string>("Twist");

                    b.HasKey("Id");

                    b.HasIndex("FermentationId")
                        .IsUnique();

                    b.HasIndex("MashTemperatureId")
                        .IsUnique();

                    b.ToTable("Methods");
                });

            modelBuilder.Entity("BurgerKing.Model.Rate", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<DateTime>("HitTime");

                    b.Property<string>("IpAddress")
                        .HasColumnType("nvarchar(32)");

                    b.HasKey("Id");

                    b.HasIndex("HitTime");

                    b.HasIndex("IpAddress");

                    b.ToTable("Rates");
                });

            modelBuilder.Entity("BurgerKing.Model.Tempreture", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<double>("Duration");

                    b.Property<int?>("MeasurementId");

                    b.HasKey("Id");

                    b.HasIndex("MeasurementId")
                        .IsUnique();

                    b.ToTable("Temperatures");
                });

            modelBuilder.Entity("BurgerKing.Model.Burger", b =>
                {
                    b.HasOne("BurgerKing.Model.Measurement", "BoilVolume")
                        .WithOne("BoilVolumeBurger")
                        .HasForeignKey("BurgerKing.Model.Burger", "BoilVolumeId");

                    b.HasOne("BurgerKing.Model.BurgerIngredients", "Ingredents")
                        .WithOne("Burger")
                        .HasForeignKey("BurgerKing.Model.Burger", "BurgerIngredientsId");

                    b.HasOne("BurgerKing.Model.Method", "Method")
                        .WithOne("Burger")
                        .HasForeignKey("BurgerKing.Model.Burger", "MethodId");

                    b.HasOne("BurgerKing.Model.Measurement", "Volume")
                        .WithOne("VolumeBurger")
                        .HasForeignKey("BurgerKing.Model.Burger", "VolumeId");
                });

            modelBuilder.Entity("BurgerKing.Model.FoodPair", b =>
                {
                    b.HasOne("BurgerKing.Model.Burger", "Burger")
                        .WithMany("FoodPairs")
                        .HasForeignKey("BurgerId");
                });

            modelBuilder.Entity("BurgerKing.Model.Ingredient", b =>
                {
                    b.HasOne("BurgerKing.Model.Measurement", "Amount")
                        .WithOne("Ingredient")
                        .HasForeignKey("BurgerKing.Model.Ingredient", "AmountId");

                    b.HasOne("BurgerKing.Model.BurgerIngredients", "BurgerIngredientHops")
                        .WithMany("Hops")
                        .HasForeignKey("BurgerIngredientHopsId");

                    b.HasOne("BurgerKing.Model.BurgerIngredients", "BurgerIngredientMalt")
                        .WithMany("Malt")
                        .HasForeignKey("BurgerIngredientMaltId");
                });

            modelBuilder.Entity("BurgerKing.Model.Method", b =>
                {
                    b.HasOne("BurgerKing.Model.Tempreture", "Fermentation")
                        .WithOne("FermentationMethod")
                        .HasForeignKey("BurgerKing.Model.Method", "FermentationId");

                    b.HasOne("BurgerKing.Model.Tempreture", "MashTemperature")
                        .WithOne("MashMethod")
                        .HasForeignKey("BurgerKing.Model.Method", "MashTemperatureId");
                });

            modelBuilder.Entity("BurgerKing.Model.Tempreture", b =>
                {
                    b.HasOne("BurgerKing.Model.Measurement", "Measurement")
                        .WithOne("Temperature")
                        .HasForeignKey("BurgerKing.Model.Tempreture", "MeasurementId");
                });
        }
    }
}
