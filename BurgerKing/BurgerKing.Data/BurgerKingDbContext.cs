﻿using System;
using BurgerKing.Model;
using Microsoft.EntityFrameworkCore;

namespace BurgerKing.Data
{
    public class BurgerKingDbContext : DbContext
    {
        public DbSet<Rate> Rates { get; set; }
        public DbSet<Tempreture> Temperatures { get; set; }
        public DbSet<Measurement> Measurements { get; set; }
        public DbSet<Method> Methods { get; set; }
        public DbSet<Ingredient> Ingredients { get; set; }
        public DbSet<BurgerIngredients> BurgerIngredients { get; set; }
        public DbSet<FoodPair> FoodPairs { get; set; }
        public DbSet<Burger> Burgers { get; set; }

        public BurgerKingDbContext(DbContextOptions options) : base(options)
        {

        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            this.BuildRates(modelBuilder);
            this.BuildTemperatures(modelBuilder);
            this.BuildMeasurements(modelBuilder);
            this.BuildMethod(modelBuilder);
            this.BuildIngredients(modelBuilder);
            this.BuildBurgerIngredients(modelBuilder);
            this.BuildFoodPair(modelBuilder);
            this.BuildBurger(modelBuilder);
        }

        private void BuildRates(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Rate>()
                .HasIndex(n => n.IpAddress);
            modelBuilder.Entity<Rate>()
                .HasIndex(n => n.HitTime);

            modelBuilder.Entity<Rate>()
               .Property(n => n.IpAddress)
               .HasColumnType("nvarchar(32)");
        }

        private void BuildTemperatures(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Tempreture>()
                .HasOne(p => p.Measurement)
                .WithOne(i => i.Temperature)
                .HasForeignKey<Tempreture>(b => b.MeasurementId);

            // TODO
        }

        private void BuildMeasurements(ModelBuilder modelBuilder)
        {
            // TODO
        }

        private void BuildMethod(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Method>()
               .HasOne(p => p.Fermentation)
               .WithOne(i => i.FermentationMethod)
               .HasForeignKey<Method>(b => b.FermentationId);

            modelBuilder.Entity<Method>()
              .HasOne(p => p.MashTemperature)
              .WithOne(i => i.MashMethod)
              .HasForeignKey<Method>(b => b.MashTemperatureId);

            // TODO
        }

        private void BuildIngredients(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Ingredient>()
               .HasOne(p => p.Amount)
               .WithOne(i => i.Ingredient)
               .HasForeignKey<Ingredient>(b => b.AmountId);

            // TODO
        }

        private void BuildBurgerIngredients(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<BurgerIngredients>()
                .HasMany(c => c.Hops)
                .WithOne(e => e.BurgerIngredientHops);

            modelBuilder.Entity<BurgerIngredients>()
                .HasMany(c => c.Malt)
                .WithOne(e => e.BurgerIngredientMalt);

            // TODO


        }

        private void BuildFoodPair(ModelBuilder modelBuilder)
        {
            // TODO
        }

        private void BuildBurger(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Burger>()
                .HasMany(c => c.FoodPairs)
                .WithOne(e => e.Burger);

            modelBuilder.Entity<Burger>()
               .HasOne(p => p.Volume)
               .WithOne(i => i.VolumeBurger)
               .HasForeignKey<Burger>(b => b.VolumeId);

            modelBuilder.Entity<Burger>()
               .HasOne(p => p.BoilVolume)
               .WithOne(i => i.BoilVolumeBurger)
               .HasForeignKey<Burger>(b => b.BoilVolumeId);

            modelBuilder.Entity<Burger>()
               .HasOne(p => p.Method)
               .WithOne(i => i.Burger)
               .HasForeignKey<Burger>(b => b.MethodId);

            modelBuilder.Entity<Burger>()
               .HasOne(p => p.Ingredents)
               .WithOne(i => i.Burger)
               .HasForeignKey<Burger>(b => b.BurgerIngredientsId);

            // TODO
        }
    }
}
