﻿using BurgerKing.Model;
using System.Collections.Generic;

namespace BurgerKing.Common
{
    public interface IBurgerDataGenerator
    {
        IEnumerable<Burger> GenerateBurgers(int count);
    }
}
