﻿using BurgerKing.Model;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using System;
using System.Linq;

namespace BurgerKing.Common
{
    public class RateCounter : IRateCounter
    {
        public int RateLimit
        {
            get
            {
                return this.configuration.GetValue<int>("App:RateLimit", 3600);
            }
        }

        public int TimeLimit
        {
            get
            {
                return this.configuration.GetValue<int>("App:TimeLimit", 1);
            }
        }

        DbContext dbContext;
        private IConfiguration configuration;

        public RateCounter(DbContext dbContext, IConfiguration configuration)
        {
            this.dbContext = dbContext;
            this.configuration = configuration;
        }

        public int RefreshRate(string ip)
        {
            int currentRate = 0;

            using (var transaction = dbContext.Database.BeginTransaction(System.Data.IsolationLevel.Snapshot))
            {
                this.DeleteOldRates();
                this.AddNewRate(ip);
                currentRate = this.CountCurrentRatings(ip);

                transaction.Commit();
            }

            return currentRate;
        }

        private void DeleteOldRates()
        {
            DateTime deadline = DateTime.Now.AddHours(-this.TimeLimit).ToUniversalTime();
            IQueryable<Rate> oldUserRates = this.dbContext.Set<Rate>().Where(r => r.HitTime < deadline);
            this.dbContext.RemoveRange(oldUserRates);

            this.dbContext.SaveChanges();
        }

        private void AddNewRate(string ip)
        {
            var rate = new Rate()
            {
                HitTime = DateTime.Now.ToUniversalTime(),
                IpAddress = ip
            };
            this.dbContext.Add(rate);

            this.dbContext.SaveChanges();
        }

        private int CountCurrentRatings(string ip)
        {
            return this.dbContext.Set<Rate>()
                .Where(r => r.IpAddress == ip)
                .Count();
        }
    }
}
