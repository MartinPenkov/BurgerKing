﻿using BurgerKing.Model;
using System;
using System.Collections.Generic;

namespace BurgerKing.Common
{
    public class SeedDataGenerator : IBurgerDataGenerator
    {
        private string[] units;
        private Random randomGenerator;

        public SeedDataGenerator()
        {
            this.randomGenerator = new Random();
            this.units = new string[] { "kg", "liter(s)", "cups", "spoons" };
        }

        public IEnumerable<Burger> GenerateBurgers(int count)
        {
            for (int i = 0; i < count; i++)
            {
                var burger = new Burger()
                {
                    Abv = GetRandomDouble(),
                    AttenuationLevel = GetRandomDouble(),
                    BoilVolume = GetRandomMeasurement(),
                    BrewersTips = "some tips " + i,
                    ContributedBy = "user" + i % 7,
                    Description = "some description " + i,
                    Ebc = GetRandomDouble(),
                    FirstBrewed = string.Format("{0}/2017", i % 12 + 1),
                    FoodPairs = new FoodPair[]
                    {
                        new FoodPair() { Value =  "some food " + i%13 },
                        new FoodPair() { Value = "some other food " + i%3 },
                    },
                    Ibu = GetRandomDouble(),
                    // Id = i,
                    ImageUrl = "http://i.dailymail.co.uk/i/pix/2015/09/29/14/2CE6A9D700000578-0-Patriotic_pride_The_teams_they_are_playing_to_decide_whether_the-m-8_1443534467003.jpg",
                    Name = "Burger king best burger " + i,
                    Ph = GetRandomDouble(),
                    Srm = GetRandomDouble(),
                    Tagline = "some tagline " + i,
                    TargetFG = GetRandomDouble(),
                    TargetOG = GetRandomDouble(),
                    Volume = GetRandomMeasurement(),
                    Method = new Method()
                    {
                        Fermentation = new Tempreture()
                        {
                            Duration = GetRandomDouble(),
                            Measurement = GetRandomMeasurement(),
                        }
                    }
                };

                var hops = new Ingredient[i % 7];
                for (int j = 0; j < i % 7; j++)
                {
                    hops[j] = GetRandomIngredient();
                }

                var malt = new Ingredient[i % 3];
                for (int j = 0; j < i % 3; j++)
                {
                    malt[j] = GetRandomIngredient();
                }

                burger.Ingredents = new BurgerIngredients()
                {
                    Yeast = "some yeast " + i,
                    Hops = hops,
                    Malt = malt,
                };

                yield return burger;
            }
        }

        private Ingredient GetRandomIngredient()
        {
            return new Ingredient()
            {
                Add = "mix",
                Amount = GetRandomMeasurement(),
                Attribute = "some attribute " + this.randomGenerator.Next(1, 12345),
                Name = "some cool name " + this.randomGenerator.Next(1, 12345),
            };
        }

        private Measurement GetRandomMeasurement()
        {
            return new Measurement()
            {
                Unit = this.units[this.randomGenerator.Next(0, 4)],
                Value = this.randomGenerator.Next(1, 26),
            };
        }

        private double GetRandomDouble()
        {
            return this.randomGenerator.Next(10, 1001) / 10.0;
        }
    }
}
