﻿namespace BurgerKing.Common
{
    public interface IRateCounter
    {
        int RateLimit { get; }
        int TimeLimit { get; }
        int RefreshRate(string ip);
    }
}
