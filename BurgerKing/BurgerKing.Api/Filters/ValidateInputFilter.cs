﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Filters;
using System.Collections.Generic;
using System.Linq;

namespace BurgerKing.Api.Filters
{
    public class ValidateInputFilter : ActionFilterAttribute
    {
        public override void OnActionExecuting(ActionExecutingContext context)
        {
            if (!context.ModelState.IsValid)
            {
                var errorMessages = Enumerable.Empty<string>();
                foreach (var value in context.ModelState.Values)
                {
                    errorMessages = errorMessages.Union(value.Errors.Select(e => e.ErrorMessage));
                }

                context.Result = new ObjectResult(null)
                {
                    StatusCode = 400,
                    Value = string.Join("\n", errorMessages.Where(e => !string.IsNullOrWhiteSpace(e))),
                };
            }
        }
    }
}
