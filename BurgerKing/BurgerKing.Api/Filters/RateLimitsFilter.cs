﻿using BurgerKing.Common;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Filters;

namespace BurgerKing.Api.Filters
{
    public class RateLimitsFilter : ActionFilterAttribute
    {
        private IRateCounter rateCounter;

        public RateLimitsFilter(IRateCounter rateCounter)
        {
            this.rateCounter = rateCounter;
        }

        public override void OnActionExecuting(ActionExecutingContext filterContext)
        {
            string ip = filterContext.HttpContext.Connection.RemoteIpAddress.ToString();
            int currentRate = this.rateCounter.RefreshRate(ip);
            this.AddHeaders(filterContext, this.rateCounter.RateLimit, currentRate);


            if (currentRate > this.rateCounter.RateLimit)
            {
                this.ReturnUnauthorizedResponse(filterContext);
            }
        }

        private void AddHeaders(ActionExecutingContext filterContext, int rateLimit, int currentRate)
        {
            IHeaderDictionary headers = filterContext.HttpContext.Response.Headers;
            headers.Add("x-ratelimit-limit", rateLimit.ToString());
            headers.Add("x-ratelimit-remaining", (rateLimit - currentRate).ToString());
        }

        private void ReturnUnauthorizedResponse(ActionExecutingContext filterContext)
        {
            filterContext.Result = new ObjectResult(null)
            {
                StatusCode = 401
            };
        }
    }
}
