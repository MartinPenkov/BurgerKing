﻿using BurgerKing.Api.Filters;
using BurgerKing.Api.Requests;
using BurgerKing.Model;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Linq;
using System.Collections.Generic;
using BurgerKing.Data;

namespace BurgerKing.Api.Controllers
{
    [Route("v2/[controller]")]
    public class BurgersController : Controller
    {
        private IRepository<Burger> burgersRepository;

        public BurgersController(IRepository<Burger> burgersRepository)
        {
            this.burgersRepository = burgersRepository;
        }

        [HttpGet]
        //[RequireHttps]
        public IEnumerable<Burger> Get(GetBurgersRequest request)
        {
            IQueryable<Burger> burgers = this.burgersRepository.GetAll();

            if (request.PerPage.HasValue && !string.IsNullOrEmpty(request.BurgerName))
            {

                burgers = burgers
                    .Where(b => b.Name.ToLower().Contains(request.BurgerName.ToLower()))
                    .Skip(request.PerPage.Value * request.Page.Value)
                        .Take(request.PerPage.Value);
            }
            else
            {
                if (!string.IsNullOrEmpty(request.BurgerName))
                {
                    burgers = burgers.Where(b => b.Name.ToLower().Contains(request.BurgerName.ToLower()));
                }

                if (request.PerPage.HasValue)
                {
                    burgers = burgers
                        .Skip(request.PerPage.Value * request.Page.Value)
                        .Take(request.PerPage.Value);
                }
            }

            return burgers.OrderBy(b => b.Id);
        }

        [HttpGet("/v2/burgers/random")]
        //[RequireHttps]
        public Burger Random()
        {
            int burgersCount = this.burgersRepository.GetAll().Count();

            var randomGenerator = new Random();
            int randomBurgerId = randomGenerator.Next(0, burgersCount);

            return this.burgersRepository.GetById(randomBurgerId);
        }

        [HttpGet("{id}")]
        //[RequireHttps]
        public ObjectResult Get(GetBurgerByIdRequest request)
        {
            Burger burger = this.burgersRepository.GetById(request.Id);
            
            if(burger == null)
            {
                return new NotFoundObjectResult("There is no burger with id " + request.Id);
            }

            return new ObjectResult(burger);
        }

        [HttpPost]
        //[RequireHttps]
        public Burger Post([FromBody]Burger request)
        {
            Burger result = this.burgersRepository.Insert(request);
            this.burgersRepository.Save();

            return result;
        }

        [Route("/error")]
        public IActionResult Error()
        {
            return new ObjectResult("Error occured. Please try again!");
        }
    }
}
