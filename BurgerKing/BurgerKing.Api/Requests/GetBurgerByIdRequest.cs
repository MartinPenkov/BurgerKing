﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BurgerKing.Api.Requests
{
    public class GetBurgerByIdRequest
    {
        public int Id { get; set; }
    }
}
