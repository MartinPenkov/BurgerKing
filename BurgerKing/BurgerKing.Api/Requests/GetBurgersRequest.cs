﻿using Microsoft.AspNetCore.Mvc;

namespace BurgerKing.Api.Requests
{
    public class GetBurgersRequest : PagedRequest
    {
        [FromQuery(Name = "burger_name")]
        public string BurgerName { get; set; } 
    }
}
