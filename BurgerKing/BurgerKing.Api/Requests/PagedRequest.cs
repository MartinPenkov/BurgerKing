﻿using Microsoft.AspNetCore.Mvc;

namespace BurgerKing.Api.Requests
{
    public abstract class PagedRequest
    {
        [FromQuery(Name ="per_page")]
        public int? PerPage { get; set; }
        [FromQuery(Name = "page")]
        public int? Page { get; set; }
    }
}
