﻿using BurgerKing.Api.Requests;
using FluentValidation;

namespace BurgerKing.Api.Validators
{
    public abstract class PagedRequestValidator<T> : AbstractValidator<T> where T: PagedRequest
    {
        public PagedRequestValidator()
        {
            RuleFor(m => m.PerPage)
                .NotNull()
                .When(m => m.Page.HasValue)
                .WithMessage("Missing per_page parameter");
            RuleFor(m => m.Page)
                .NotNull()
                .When(m => m.PerPage.HasValue)
                .WithMessage("Missing page parameter");

            RuleFor(m => m.PerPage)
                .GreaterThan(0)
                .When(m => m.PerPage.HasValue)
                .WithMessage("The parameter per_page should be a positive integer");
            RuleFor(m => m.Page)
                .GreaterThan(0)
                .When(m => m.Page.HasValue)
                .WithMessage("The parameter page should be a positive integer");
        }
    }
}
