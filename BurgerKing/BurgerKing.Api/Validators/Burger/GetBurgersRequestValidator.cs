﻿using BurgerKing.Api.Requests;
using FluentValidation;

namespace BurgerKing.Api.Validators
{
    public class GetBurgersRequestValidator : PagedRequestValidator<GetBurgersRequest>
    {
        public GetBurgersRequestValidator()
        {
            RuleFor(m => m.BurgerName)
                .NotEmpty()
                .When(m => m.BurgerName != null)
                .WithMessage("Please enter a nonempty burger name");
        }
    }
}
