﻿using BurgerKing.Api.Requests;
using BurgerKing.Model;
using FluentValidation;

namespace BurgerKing.Api.Validators
{
    public class PostBurgerRequestValidator : AbstractValidator<Burger>
    {
        public PostBurgerRequestValidator()
        {
            RuleFor(r => r.Name)
                .NotEmpty()
                .WithMessage("Name is required!");
        }
    }
}
