﻿using BurgerKing.Api.Requests;
using FluentValidation;

namespace BurgerKing.Api.Validators
{
    public class GetBurgerByIdRequestValidator : AbstractValidator<GetBurgerByIdRequest>
    {
        public GetBurgerByIdRequestValidator()
        {
            RuleFor(r => r.Id)
                .GreaterThan(0)
                .WithMessage("The id must be positive");
        }
    }
}
