﻿using BurgerKing.Api.Filters;
using BurgerKing.Common;
using BurgerKing.Data;
using BurgerKing.Model;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using FluentValidation.AspNetCore;
using System;

namespace BurgerKing.Api
{
    public class Startup
    {
        public IConfigurationRoot Configuration { get; }

        public Startup(IHostingEnvironment env)
        {
            var builder = new ConfigurationBuilder()
                .SetBasePath(env.ContentRootPath)
                .AddJsonFile("appsettings.json", optional: false, reloadOnChange: true)
                .AddJsonFile($"appsettings.{env.EnvironmentName}.json", optional: true)
                .AddEnvironmentVariables();
            Configuration = builder.Build();
        }

        public void ConfigureServices(IServiceCollection services)
        {
            services.AddDbContext<BurgerKingDbContext>(options =>
               options.UseSqlServer(Configuration.GetConnectionString("DefaultConnection")));

            services.AddScoped<IRepository<Burger>, BurgerRepository>(
                s => new BurgerRepository(s.GetRequiredService<BurgerKingDbContext>())
            );

            services.AddSingleton<IConfiguration>(Configuration);
            services.AddScoped<IRateCounter>(s => new RateCounter(
                s.GetRequiredService<BurgerKingDbContext>(),
                s.GetRequiredService<IConfiguration>()
                )
            );

            services.AddCors();
            services.AddMvc(options =>
                {
                    options.Filters.Add(typeof(RateLimitsFilter));
                    options.Filters.Add(typeof(ValidateInputFilter));
                })
                .AddFluentValidation(fvc => fvc.RegisterValidatorsFromAssemblyContaining<Startup>());

            services.AddScoped<RateLimitsFilter>();
            services.AddScoped<ValidateInputFilter>();

            services.AddTransient<IBurgerDataGenerator, SeedDataGenerator>();
        }

        public void Configure(IApplicationBuilder app, IHostingEnvironment env, ILoggerFactory loggerFactory)
        {
            app.UseExceptionHandler("/error");
            app.UseMvc();

            var serviceScopeFactory = app.ApplicationServices.GetRequiredService<IServiceScopeFactory>();
            using (var serviceScope = serviceScopeFactory.CreateScope())
            {
                var dbContext = serviceScope.ServiceProvider.GetService<BurgerKingDbContext>();
                dbContext.Database.Migrate();

                dbContext.Database.OpenConnection();
                dbContext.Database.ExecuteSqlCommand("ALTER DATABASE BurgerKing SET READ_COMMITTED_SNAPSHOT ON");
                dbContext.Database.ExecuteSqlCommand("ALTER DATABASE BurgerKing SET ALLOW_SNAPSHOT_ISOLATION ON");

                IRepository<Burger> burgersRepo = serviceScope.ServiceProvider.GetService<IRepository<Burger>>();
                if (burgersRepo.GetById(1) == null)
                {
                    this.SeedData(serviceScope.ServiceProvider.GetService<IBurgerDataGenerator>(), burgersRepo);
                }
            }
        }

        private void SeedData(IBurgerDataGenerator dataGenerator, IRepository<Burger> burgersRepo)
        {
            foreach (Burger burger in dataGenerator.GenerateBurgers(100))
            {
                burgersRepo.Insert(burger);
                burgersRepo.Save();
            }
        }
    }
}

